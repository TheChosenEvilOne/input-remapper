#ifdef X11
#warning "Compiling for X11 KeyEvents."
#else
#warning "Compiling for /dev/input/event*."
#endif


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <linux/input.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>

#ifdef X11
#include <X11/Xlib.h>
#include "utf.h"
#endif

#define UNGRAB    0
#define GRAB      1

unsigned short m_cnt;
#ifdef X11
struct x11_mapping {
	unsigned short from;
	KeySym to;
	unsigned int state;
};
struct x11_mapping* mappings;
#else
struct mapping {
	unsigned short from;
	unsigned short to;
};
struct mapping* mappings;
#endif

const char* device = "/dev/input/event";

char* conf_file;
char* pad_device;
char* key_device;

#ifdef X11
struct ll {
	struct ll* next;
	int i;
};
struct ll* dl;
#endif

void parseconf(char* file) {
	FILE *fp = fopen(file, "r");
	if (!fp) {
		printf("Couldn't open configuration file %s: %s.\n", file, strerror(errno));
		return;
	}
	short cnt = 0, cnt2 = 0;
	char c;
#ifdef X11
	int ctrl, cl = 0;
	char lc;
	struct ll* cr;
	cr = dl = calloc(1, sizeof(struct ll));
#endif
	while (fread(&c, 1, 1, fp) > 0) {
		cnt += c == ':';
#ifdef X11	
		cl += c == '\n';
		if (c == '-' && lc != '\\') {
			cr->i = cl + 1;
			cr = (cr->next = calloc(1, sizeof(struct ll)));
		}
		lc = c;
#endif
	}
	m_cnt = cnt;
	if (mappings)
		free(mappings);
#ifdef X11
	mappings = calloc(1, cnt * sizeof(struct x11_mapping));
#else
	mappings = calloc(1, cnt * sizeof(struct mapping));
#endif
	rewind(fp);
	cnt = 0, cnt2 = 0;
	char* buf = calloc(1, 256);
#ifdef X11
	char ste = 0;
	cl = 0;
	cr = dl;
	while (fread(&c, 1, 1, fp) > 0) {
		cl += c == '\n';
		if (ste == 0) {
			if (isdigit(c)) {
				buf[cnt++] = c;
			} else if (c == ':') {
				mappings[cnt2].from = (unsigned short) atoi(buf);
				memset(buf, 0, cnt), cnt = 0;
				if (dl && dl->i == cl + 1) {
					ste = 1;
					if (dl->next)
						dl = dl->next;
				} else {
					ste = 2;
				}
			}
			continue;
		}

		if (ste == 1) {
			if (c == '-') {
				ste = 3;
			} else if (c == 'M') {
				mappings[cnt2].state |= Mod1Mask;
			} else if (c == 'S') {
				mappings[cnt2].state |= Mod4Mask;
			} else if (c == '^') {
				mappings[cnt2].state |= ShiftMask;
			} else if (c == 'C') {
				mappings[cnt2].state |= ControlMask;
			}
			continue;
		}

		if (ste == 2) {
			if (c == ' ')
				continue;
			ste = 3;
		}

		if (c == '\n') {
			dl->next && (dl = dl->next);
			uint32_t chr = utf8_to_32(buf);

			// XStringToKeysym SHOULD work fine with values up to 256.
			if (chr > 0x100) {
				mappings[cnt2++].to = chr | 0x01000000;
			} else {
				mappings[cnt2++].to = XStringToKeysym(buf);
			}
			memset(buf, 0, cnt), cnt = 0;
			ste = 0;
			continue;
		}
		buf[cnt++] = c;
	}
#else
	while (fread(&c, 1, 1, fp) > 0) {
		if (isdigit(c)) {
			buf[cnt++] = c;
			continue;
		} else if (c == ':') {
			mappings[cnt2].from = (unsigned short) atoi(buf);
			memset(buf, 0, cnt), cnt = 0;
		} else if (c == '\n') {
			mappings[cnt2++].to = (unsigned short) atoi(buf);
			memset(buf, 0, cnt), cnt = 0;
		}	 
	}
#endif
	free(buf);
}

void siguser_handler(int signum) {
	parseconf(conf_file);
}

int main(int argc, char** argv) {
#ifdef X11
	printf("Currently compiled to use X11 KeyEvents.\n");
#else
	printf("Currently compiled to directly use /dev/input/event* devices.\n");
#endif
#ifdef X11
	if (argc != 3) {
		printf("This program requires TWO (2) arguments: <configuration file> <input event device>.\n");
#else
	if (argc != 4) {
		printf("This program requires THREE (3) arguments: <configuration file> <input event device> <output event device>.\n");
#endif
		return 0;
	}

	if (access(argv[1], F_OK) < 0) {
		printf("Couldn't access configuration file %s: %s.\n", argv[1], strerror(errno));
		return -1;
	}
#ifdef X11
	if (!atoi(argv[2])) {
		printf("Argument two MUST be a number. Got: %s.\n", argv[2]);
#else
	if (!atoi(argv[2]) || !atoi(argv[3])) {
		printf("Arguments two and three MUST be numbers. Got: %s %s.\n", argv[2], argv[3]);
#endif
		return -1;
	}

	// the number should never be longer than 2 characters but still.
	char buff[8];
	sprintf(&buff[0], "%d", atoi(argv[2]));
	pad_device = malloc(strlen(device) + strlen(buff) + 1);
	strcpy(pad_device, device);
	strcat(pad_device, buff);

#ifndef X11	
	sprintf(&buff[0], "%d", atoi(argv[3]));
	key_device = malloc(strlen(device) + strlen(buff) + 1);
	strcpy(key_device, device);
	strcat(key_device, buff);
#endif

	conf_file = argv[1];

	struct sigaction sa;
	sa.sa_handler = siguser_handler;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGUSR1, &sa, NULL) < 0 && printf("Couldn't register SIGUSR1 handler: %s.", strerror(errno));

	parseconf(argv[1]);

	int fail = 0;

	int pfd = open(pad_device, O_RDONLY);
	pfd < 0 && (printf("Couldn't open %s: %s.\n", pad_device, strerror(errno)), fail = 1);

#ifndef X11
	int kfd = open(key_device, O_RDWR);
	kfd < 0 && (printf("Couldn't open %s: %s.\n", key_device, strerror(errno)), fail = 1);
#endif

	ioctl(pfd, EVIOCGRAB, GRAB) < 0 && (printf("Couldn't grab %s: %s.\n", pad_device, strerror(errno)), fail = 1);
	if (fail) return -1;

#ifdef X11
	Display* d = XOpenDisplay(NULL);
	int kc_low, kc_high, ks_c, sch, tmp;
	XDisplayKeycodes(d, &kc_low, &kc_high);
	KeySym* ks = XGetKeyboardMapping(
    		d, 
    		kc_low, 
    		kc_high - kc_low, 
    		&ks_c);

	for (sch = kc_high; sch >= kc_low; sch--) {
		for (int b = 0; b < ks_c; b++) {
			if (ks[(sch - kc_low) * ks_c + b])
				break;
			goto out;
		}
	}
out:
	Window wroot = XDefaultRootWindow(d);
	XKeyEvent xev;
	xev.same_screen = 1;
#else
	struct input_event esyn;
	esyn.type = EV_SYN;
	esyn.code = esyn.value = 0;
#endif

	struct input_event ev;
	while (1) {
    		read(pfd, &ev, sizeof(ev));
    		if (ev.type != EV_KEY)
			continue;
		int cod = ev.code;
		for (unsigned short i = 0; i < m_cnt; i++) {
			if (mappings[i].from != cod)
				continue;
#ifdef X11
			KeySym sl[] = { mappings[i].to };
			XChangeKeyboardMapping(d, sch, 1, sl, 1);
			Window w;
			XGetInputFocus(d, &w, &tmp);
			XSync(d, 0);
			XKeyEvent xev;
			xev.display = d;
			xev.window = w;
			xev.same_screen = 1;
			xev.subwindow = None;
			xev.time = CurrentTime;
			xev.root = wroot;
			xev.x = xev.y = xev.x_root = xev.y_root = 1;
			xev.type = ev.value ? KeyPress : KeyRelease;
			xev.state = mappings[i].state;
			xev.keycode = sch;
			XSendEvent(d, InputFocus, 1, KeyPressMask, (XEvent*) &xev);
			XFlush(d);
			break;
#else
			ev.code = mappings[i].to;
			write(kfd, &ev, sizeof(ev));
			write(kfd, &esyn, sizeof(esyn));
			// No break to allow sending multiple keys with a single press.
			//break;
#endif
    		}
  	}
}
