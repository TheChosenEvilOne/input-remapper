#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

size_t utf8_charlen(uint8_t c) {
	if (c < 0x80)
		return 1;
	else if ((c & 0xe0) == 0xc0)
		return 2;
	else if ((c & 0xf0) == 0xe0)
		return 3;
	else if ((c & 0xf8) == 0xf0 && (c <= 0xf4))
		return 4;
	return 0;
}

uint32_t utf8_to_32(const uint8_t *c) {
	switch(utf8_charlen(*c)) {
	        case 0:
			return 0;
	        case 1:
			return *c;
	        case 2:
			return ((c[0] & 0x1f) << 6)
				| (c[1] & 0x3f);
        	case 3:
			return ((c[0] & 0x0f) << 12)
				| ((c[1] & 0x3f) << 6)
				| (c[2] & 0x3f);
        	case 4:
			return ((c[0] & 0x07) << 18)
				| ((c[1] & 0x3f) << 12)
				| ((c[2] & 0x3f) << 6)
				| (c[3] & 0x3f);
    	}
	return 0;
}
